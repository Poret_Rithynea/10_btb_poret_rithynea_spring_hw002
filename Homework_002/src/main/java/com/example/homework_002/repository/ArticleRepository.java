package com.example.homework_002.repository;

import com.example.homework_002.model.Article;
import org.springframework.stereotype.Repository;
import java.util.ArrayList;
import java.util.List;
@Repository
public class ArticleRepository {
      List<Article> articles = new ArrayList<>();

      public ArticleRepository() {
          articles.add(new Article(1, "Spring", "Spring makes programming Java quicker, easier, and safer for everybody. Spring's\n" +
                  "focus on speed, simplicity, and productivity has made it the world's most popular\n" +
                  "Java framework.", "https://4.bp.blogspot.com/-9kYSwCDRbms/W-qSUvwnFWI/AAAAAAAAEsE/j4EeFEPQHBc-QpxMV9l3gQAaLAuG2WhTgCLcBGAs/s1600/spring-framework.png"));
          articles.add(new Article(2, "Docker", "Docker takes away repetitive, mundane configuration tasks and is used throughout\n" +
                  "the development lifecycle for fast, easy and portable application development-\n" +
                  "desktop and cloud. Docker's comprehensive end to end platform includes Uls,\n" +
                  "CLIS, APIS and security that are engineered to work together across the entire\n" +
                  "application delivery lifecycle.", "https://4.bp.blogspot.com/-9kYSwCDRbms/W-qSUvwnFWI/AAAAAAAAEsE/j4EeFEPQHBc-QpxMV9l3gQAaLAuG2WhTgCLcBGAs/s1600/spring-framework.png"));
          articles.add(new Article(3, "Linux", "Linux isafamily of open-source Unix-like operating systems based on the Linux\n" +
                  "kernel, an operating system kernel first released on September 17, 1991, by Linus\n" +
                  "Torvalds. Linux is typically packaged inaLinux distribution.", "https://4.bp.blogspot.com/-9kYSwCDRbms/W-qSUvwnFWI/AAAAAAAAEsE/j4EeFEPQHBc-QpxMV9l3gQAaLAuG2WhTgCLcBGAs/s1600/spring-framework.png"));
      }
    public List<Article> getAllArticles(){
        return articles;
    }

    public void InsertArticle(Article article){
        article.setId(articles.size()+1);
        articles.add(article);
    }
    public Article findArticleByID(int id){
          return articles.stream().filter(article -> article.getId()== id).findFirst().orElseThrow();
    }

    public void deleteArticle(int id){
        for(int i =0; i<articles.size();i++)
        {
            if(id==articles.get(i).getId())
            {
                articles.remove(i);
            }
        }
    }

}
