package com.example.homework_002.service.serviceImp;

import com.example.homework_002.model.Article;
import com.example.homework_002.repository.ArticleRepository;
import com.example.homework_002.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImp implements ArticleService {
    @Autowired
    ArticleRepository articleRepository;

    @Override
    public List<Article> getAllArticle() {
        return articleRepository.getAllArticles();
    }

    @Override
    public void articleMethod() {
        System.out.println("Version one the student method...");
    }

    @Override
    public Article findArticleByID(int id) {
        return articleRepository.findArticleByID(id);
    }

    @Override
    public void addArticleMethod(Article article) {
        articleRepository.InsertArticle(article);
    }

    @Override
    public void DedeleteArticleByID(int id) {
         articleRepository.deleteArticle(id);
    }

}
