package com.example.homework_002.service;

import com.example.homework_002.model.Article;

import java.util.List;

public interface ArticleService {
    public List<Article> getAllArticle();

    public Article findArticleByID(int id);
    public void articleMethod();
    public void addArticleMethod(Article article);

    public void DedeleteArticleByID(int id);
}
