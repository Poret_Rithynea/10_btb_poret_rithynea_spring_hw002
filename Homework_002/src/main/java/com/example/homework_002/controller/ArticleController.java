package com.example.homework_002.controller;

import com.example.homework_002.model.Article;
import com.example.homework_002.repository.ArticleRepository;
import com.example.homework_002.service.ArticleService;
import com.example.homework_002.service.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.io.IOException;

@Controller
public class ArticleController {

@Autowired
@Qualifier("articleServiceImp")

ArticleService articleService;

@Autowired
FileStorageService fileStorageService;

    @GetMapping
    public String index(Model model){
        model.addAttribute("article", new Article());
        model.addAttribute("articles",articleService.getAllArticle());
        return "index";
    }
    @GetMapping("/form-add")
    public String showFormAdd(Model model){
        model.addAttribute("article",new Article());
        return "form-add";
    }


    @PostMapping("/handle-add")
    public  ModelAndView handleAdd(
            @RequestParam("file") MultipartFile file,
            @Valid @ModelAttribute("newArticle") Article article,
            BindingResult bindingResult
    ){
        if (bindingResult.hasErrors()) {
            ModelAndView mv = new ModelAndView();
            System.out.println(" Code here it works : ");
            mv.setViewName("form-add");
            return mv;
        }
            try{
                System.out.println("File :  "+article.getFile().getContentType());
                String filename = "http://localhost:1234/images/"+fileStorageService.saveFile(article.getFile());
                System.out.println("filename: "+filename);
                article.setProfile(filename);

            }catch (IOException ex){
                System.out.println("Error with the image upload "+ex.getMessage());
            }
        System.out.println("Here is result value : "+article);

        articleService.addArticleMethod(article);
        ModelAndView mv = new ModelAndView();
        mv.setViewName("redirect:/");
        return mv;
    }
    @GetMapping("/view-article/{id}")
    public String viewArticel(@PathVariable int id, Model model){
        // Find Student By ID

        Article articleResult = articleService.findArticleByID(id);

        System.out.println("article result : "+articleResult);
        // send value to the view
        model.addAttribute("article",articleResult);
        return "form-view";
    }

    @GetMapping("/delete-article/{id}")
    public String deleteArticle(@PathVariable int id, @ModelAttribute("article") Article article){
        articleService.DedeleteArticleByID(id);
        return "redirect:/";
    }
}